package yuanxin.solr.generator.service;

import com.baomidou.mybatisplus.extension.service.IService;
import yuanxin.solr.generator.entity.TableInfo;
import org.springframework.stereotype.Service;


/**
 * @author huyuanxin
 */
@Service
public interface TableInfoService extends IService<TableInfo> {

}

